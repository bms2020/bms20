import logging
import time
from operator import eq

from selenium import webdriver
import requests
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
import re
from selenium.webdriver.common.keys import Keys
from datetime import datetime
import pymysql

"""
0410 남은작업 : 채소외것들도 검색하면 동작할수있게 Click 처리 

"""


def Main():
    Search_list = ['감자', '오이', '쌀', '달걀', '바나나']
    options = webdriver.ChromeOptions()
    driver = webdriver.Chrome("D:\python\chromedriver.exe", options=options)
    #driver = webdriver.Chrome(D:\python\lib", options=options)
    driver.implicitly_wait(5)
    driver.get("http://www.ssg.com/")
    time.sleep(1)

    # 페이지로드 완료
    # key_search = Search_list[1]
    for key_search in Search_list:
        sKeyword = driver.find_element_by_xpath('//*[@id="ssg-query"]')
        fButton = driver.find_element_by_xpath('//*[@id="ssg-query-btn"]')
        sKeyword.clear()
        sKeyword.click()
        sKeyword.send_keys(key_search)
        fButton.click()
        time.sleep(1)

        # 카테고리도 검색어에따라서 변함 ......
        # 카테고리 클릭
        # 쌀, 달걀

        if eq(key_search, "쌀") or eq(key_search, "달걀"):

            driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[1]/a').click()
            time.sleep(1)

            if eq(key_search, "쌀"):
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li[1]/a').click()
                time.sleep(1)

            if eq(key_search, "달걀"):
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li[1]/a').click()
                time.sleep(1)
        # 바나나 ,오이 , 감자
        if eq(key_search, "바나나") or eq(key_search, "오이") or eq(key_search, "감자"):

            driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/a').click()
            time.sleep(1)
            if eq(key_search, "바나나"):
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li[1]/a').click()
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li/ul/li[1]/a').click()
            if eq(key_search, "오이"):
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li[1]/a').click()
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li/ul/li[1]/a').click()
            if eq(key_search, "감자"):
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li[1]/a').click()
                time.sleep(1)
                driver.find_element_by_xpath('//*[@id="search_category_list"]/ul/li[2]/ul/li/ul/li[1]/a').click()

        time.sleep(2)

        # 검색완료

        # Url 이 몇개까지 있는지 ?
        # a 태그안에 어트리뷰트를 가져와야됨
        first_page = driver.find_element(By.ID, 'item_navi').find_elements_by_tag_name('strong')[0].text
        # 페이지가 리스트가있는지
        last_page_list = driver.find_element(By.ID, 'item_navi').find_elements_by_tag_name('a')
        # page 가 하나인경우
        if len(last_page_list) == 0:
            current_url = driver.current_url
            url = current_url + "&page=" + str(first_page)
            print(url)
            # url 전달해서 크롤링 시작
            get_crawling_data(url, key_search)
        # Page 가 여러개인 경우
        else:
            last_page = last_page_list.pop()
            last_page = str(last_page.get_attribute('data-filter-value'))
            # 숫자만 추출
            last_page = int(last_page[5:])
            current_url = driver.current_url
            for page in range(1, last_page + 1):
                url = current_url + "&page=" + str(page)
                print(url)
                # url 전달해서 크롤링 시작
                get_crawling_data(url, key_search)


def get_crawling_data(url, key_search):
    # 여기서 페이지에서 가져온거를 Check 함수로 넘김
    res = requests.get(url)
    html = res.text
    soup = BeautifulSoup(html, 'html.parser')
    # 전체를일단 가져와서
    all_list = soup.select("div.tmpl_itemlist > div > ul > li")
    parsing_list = []
    for item in all_list:
        parsing_list.append(item)

    if eq(key_search, "감자"):
        # 검사 완료 (필요없는 항목 Parsing)
        name_parsing_list = Check_name_potato(parsing_list, key_search)
        # DB Write
        DB_Add(name_parsing_list)
    if eq(key_search, "오이"):
        name_parsing_list = Check_name_cucumber(parsing_list, key_search)
        DB_Add(name_parsing_list)
    if eq(key_search, "쌀"):
        name_parsing_list = Check_name_rice(parsing_list, key_search)
        DB_Add(name_parsing_list)
    if eq(key_search, "달걀"):
        name_parsing_list = Check_name_egg(parsing_list, key_search)
        DB_Add(name_parsing_list)
    if eq(key_search, "바나나"):
        name_parsing_list = Check_name_banana(parsing_list, key_search)
        DB_Add(name_parsing_list)


def Check_name_potato(parsing_list, key_search):
    # 제목 검사
    # 리스트를 받아서검사하고 검사완료된리스트를 넘겨줘야하나
    check_end_name_list = []
    for name in parsing_list:
        # 특정 문자를 찾았을때 없으면 -1
        # 여기서 제목을 받지못하면 다음 객체로 넘어감
        # 감자라는 단어가 없으면
        # 포함되어야되는 단어
        if name.select('a>em.tx_ko')[0].text.find(key_search) == -1:
            continue

        # 빼야되는 단어
        if name.select('a>em.tx_ko')[0].text.find("과자") != -1:
            continue
        # 큐브라는 단어가 포함되어있으면 처음으로
        if name.select('a>em.tx_ko')[0].text.find("큐브") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("깐감자") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("샐러드") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("조림감자") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("감자환") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("가루") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("티백") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("차") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("구운") != -1:
            continue

        check_end_name_list.append(name)

    return check_end_name_list


def Check_name_cucumber(parsing_list, key_search):
    # 제목 검사
    # 리스트를 받아서검사하고 검사완료된리스트를 넘겨줘야하나
    check_end_name_list = []
    for name in parsing_list:
        # 특정 문자를 찾았을때 없으면 -1
        # 여기서 제목을 받지못하면 다음 객체로 넘어감
        # 감자라는 단어가 없으면
        # 포함되어야되는 단어
        if name.select('a>em.tx_ko')[0].text.find(key_search) == -1:
            continue
        # 빼야되는 단어
        if name.select('a>em.tx_ko')[0].text.find("오이지") != -1:
            continue

        check_end_name_list.append(name)

    return check_end_name_list


def Check_name_banana(parsing_list, key_search):
    # 제목 검사
    # 리스트를 받아서검사하고 검사완료된리스트를 넘겨줘야하나
    check_end_name_list = []
    for name in parsing_list:
        # 특정 문자를 찾았을때 없으면 -1
        # 여기서 제목을 받지못하면 다음 객체로 넘어감
        # 감자라는 단어가 없으면
        # 포함되어야되는 단어
        if name.select('a>em.tx_ko')[0].text.find(key_search) == -1:
            continue
        # 빼야되는 단어
        if name.select('a>em.tx_ko')[0].text.find("칩") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("행사") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("모음전") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("냉동") != -1:
            continue

        check_end_name_list.append(name)

    return check_end_name_list


def Check_name_egg(parsing_list, key_search):
    # 제목 검사
    # 리스트를 받아서검사하고 검사완료된리스트를 넘겨줘야하나
    check_end_name_list = []
    for name in parsing_list:
        # 특정 문자를 찾았을때 없으면 -1
        # 포함되어야되는 단어
        # 달걀이라는 단어가없음..
        # if name.select('a>em.tx_ko')[0].text.find(key_search) == -1:
        #   continue

        # 빼야되는 단어
        if name.select('a>em.tx_ko')[0].text.find("구운") != -1:
            continue
        # 큐브라는 단어가 포함되어있으면 처음으로
        if name.select('a>em.tx_ko')[0].text.find("훈제") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("반숙") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("메추리") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("옵션") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("모음전") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("군계란") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("요리란") != -1:
            continue

        check_end_name_list.append(name)

    return check_end_name_list


def Check_name_rice(parsing_list, key_search):
    # 제목 검사
    # 리스트를 받아서검사하고 검사완료된리스트를 넘겨줘야하나
    check_end_name_list = []
    for name in parsing_list:
        # 특정 문자를 찾았을때 없으면 -1
        # 여기서 제목을 받지못하면 다음 객체로 넘어감
        # 감자라는 단어가 없으면
        # 포함되어야되는 단어
        if name.select('a>em.tx_ko')[0].text.find(key_search) == -1:
            continue

        # 빼야되는 단어
        if name.select('a>em.tx_ko')[0].text.find("분말") != -1:
            continue
        # 큐브라는 단어가 포함되어있으면 처음으로

        if name.select('a>em.tx_ko')[0].text.find("밥") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("현미") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("누룽지") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("잡곡") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("쌀눈") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("찹쌀") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("보리") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("곤약쌀") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("뻥튀기") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("좁쌀") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("녹두") != -1:
            continue
        if name.select('a>em.tx_ko')[0].text.find("귀리") != -1:
            continue
        check_end_name_list.append(name)

    return check_end_name_list


def DB_Add(name_parsing_list):
    global curs
    conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='1234', db='crawling', charset='utf8')
    try:
        curs = conn.cursor()
        for pars in name_parsing_list:
            # 이름
            name = pars.select('a>em.tx_ko')[0].text
            # 가격
            price = pars.select('em.ssg_price')[0].text
            # 개당 값 검사
            if str(pars.select('div.unit')) == '[]':
                unit = None
            else:
                unit = pars.select('div.unit')[0].text
                unit = unit.replace('(', '')
                unit = unit.replace(')', '')
            # 평점 검사
            if str(pars.select('div.rate_bg')) == '[]':
                score = None
            else:
                score = pars.select('div.rate_bg')[0].text
                score = score.strip()
                score = score.replace('별점', '')
                score = score.replace('점', '')
            # 댓글 검사
            if str(pars.select('span.rate_tx')) == '[]':
                comment = None
            else:
                comment = pars.select('span.rate_tx')[0].text  # 댓글수
                comment = comment.replace('(', '')
                comment = comment.replace(')', '')

            sql = """insert into """ + \
                  """tbl_ssg(name,price,unit,score,comment,date)values(%s,%s,%s,%s,%s,%s)"""

            dateTimeObj = datetime.now()
            timestampStr = dateTimeObj.strftime("%Y-%m-%d").strip()
            DBdata = (name, price, unit, score, comment, timestampStr)
            print(DBdata)
            curs.execute(sql, DBdata)
            conn.commit()
    except Exception as e:
        print(e)
    finally:
        curs.close()
        conn.close()


if __name__ == "__main__":
    Main()

"""
    # name_list = soup.find("div", class_="cunit_lst_v").find_all("em", class_="tx_ko")
    # price_list = soup.find("div", class_="cunit_lst_v").find_all("em", class_="ssg_price")
    # price_unit_list = soup.find("div", class_="cunit_lst_v").find_all("div", class_="unit")
    # score_list = soup.find("div", class_="cunit_lst_v").find_all("div", class_='rate_bg')
    # commnent_list = soup.find("div", class_="cunit_lst_v").find_all("span", class_='rate_tx')

   # #item_unit_0000010015897 >
    # idProductImg
    # finance_html = soup.select('#idProductImg > li ')
    # test=soup.select("div.tmpl_itemlist > div > ul > li")
    # print(test)
    
    # score = pars.select('div.rate_bg')[0].text #평점
    # comment =pars.select('span.rate_tx')[0].text # 댓글수
"""
